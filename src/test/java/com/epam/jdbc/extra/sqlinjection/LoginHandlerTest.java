package com.epam.jdbc.extra.sqlinjection;

import com.epam.jdbc.extra.ConnectionSource;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.OptionalLong;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class LoginHandlerTest {

    @Test
    void testLoginHandlerSqlInjection() throws SQLException {

        ConnectionSource connectionSource = new ConnectionSource("sql-injection");
        LoginHandler loginHandler = new LoginHandler(connectionSource.getConnection());

        OptionalLong userId = loginHandler.login("' OR USER_ID = 100 --", "");

        //assertions pass - password is right
        assertTrue(userId.isPresent());
        assertEquals(200L, userId.getAsLong());
    }

}