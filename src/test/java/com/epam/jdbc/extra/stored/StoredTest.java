package com.epam.jdbc.extra.stored;

import com.epam.jdbc.extra.ConnectionSource;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

class StoredTest {
    @Test
    void testLoginHandlerSqlInjection() throws SQLException {

        ConnectionSource connectionSource = new ConnectionSource("stored");
        try (Connection connection = connectionSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(
                     "select USER_ID, USERNAME, USERPASS, IS_ADMIN(USERNAME) " +
                             "from USER");
             ResultSet resultSet = ps.executeQuery()) {
            while (resultSet.next()) {
                System.out.println(resultSet.getLong(1) + " - "
                        + resultSet.getString(2) + " - "
                        + resultSet.getString(3) + " - "
                        + resultSet.getBoolean(4));
            }
        }
    }
}