package com.epam.jdbc.extra.lobs;

import com.epam.jdbc.extra.ConnectionSource;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class DemoLobsTest {

    @Test
    void testDemoLobs() throws Exception {
        final ConnectionSource connectionSource = new ConnectionSource("lobs");
        try (Connection conn = connectionSource.getConnection();
             PreparedStatement ps = conn.prepareStatement("INSERT INTO IMAGE VALUES ( ?, ?, ?)")) {
            ps.setInt(1, 1);
            ps.setString(2, "cat.jpg");
            ps.setBinaryStream(3, new FileInputStream("src/test/resources/cat.jpg"));

            assertEquals(1, ps.executeUpdate());
        }

        try (Connection conn = connectionSource.getConnection();
             PreparedStatement ps = conn.prepareStatement("INSERT INTO IMAGE VALUES ( ?, ?, ?)")) {
            ps.setInt(1, 2);
            ps.setString(2, "cat2.jpg");
            final Blob blob = conn.createBlob();
            blob.setBytes(1, Files.readAllBytes(Paths.get("src/test/resources/cat.jpg")));
            ps.setBlob(3, blob);
            assertEquals(1, ps.executeUpdate());
        }

        try (Connection conn = connectionSource.getConnection();
             PreparedStatement ps = conn.prepareStatement("SELECT * FROM IMAGE");
             ResultSet resultSet = ps.executeQuery()) {

            while (resultSet.next()) {
                String name = resultSet.getString(2);
                InputStream binaryStream = resultSet.getBinaryStream(3);
                byte[] bytes = IOUtils.toByteArray(binaryStream);
                Files.write(Paths.get(name), bytes);
            }

        }
    }

}