package com.epam.jdbc.extra.daorepo;

import com.epam.jdbc.extra.ConnectionSource;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void test() throws Exception {

        UserDao userDao = new UserDaoJdbcImpl(new ConnectionSource("dao-repo"));
//        userDao.create(new User(100, "ADMIN", "ADMINP@SS"));
//        userDao.create(new User(200, "Edgar", "P@wP@w"));
//        userDao.create(new User(300, "Bernard", "Sh@wSh@w"));
//        userDao.create(new User(400, "John", "D0wD0w"));

        userDao.delete(400);
        final User edgar = userDao.read(200);
        edgar.setPassword("@l@n");
        userDao.update(edgar);

        System.out.println(userDao.read(200));
        System.out.println(userDao.read(400));
    }

}