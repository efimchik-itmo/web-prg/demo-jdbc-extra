package com.epam.jdbc.extra;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;
import java.util.stream.Collectors;

public class ConnectionSource {

    public static final String URL = "jdbc:h2:mem:%s;DB_CLOSE_DELAY=-1";
    public static final String USER = "sa";
    public static final String PASSWORD = "";

    private final String dbName;

    public ConnectionSource(final String dbName) throws SQLException {
        this.dbName = dbName;

        try (Connection conn = getConnection()) {
            try (Statement statement = conn.createStatement()) {
                statement.execute(getSql("ddl-" + dbName + ".sql"));
            }
            try (Statement statement = conn.createStatement()) {
                statement.execute(getSql("dml-" + dbName + ".sql"));
            }
        }
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(String.format(URL, dbName), USER, PASSWORD);
    }

    private static String getSql(final String resourceName) {
        return new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(
                                ConnectionSource.class.getClassLoader().getResourceAsStream(resourceName))))
                .lines()
                .collect(Collectors.joining("\n"));
    }

}
