package com.epam.jdbc.extra.daorepo;

public class User {
    private long id;
    private String username;
    private String password;

    public User(final long id, final String username, final String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "" + id +
                ", " + username +
                ", " + password +
                '}';
    }
}
