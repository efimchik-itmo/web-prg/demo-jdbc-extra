package com.epam.jdbc.extra.daorepo;

public interface UserRepository {
    User get(Long id);

    void add(User user);

    void update(User user);

    void remove(User user);
}
