package com.epam.jdbc.extra.daorepo;

import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    List<User> users = new ArrayList<>();

    @Override
    public void create(final User user) {
        users.add(user);
    }

    @Override
    public User read(final long id) {
        return users.stream()
                .filter(u -> u.getId() == id)
                .findAny().orElse(null);
    }

    @Override
    public void update(final User user) {

    }

    @Override
    public void delete(final long id) {
        users.remove(read(id));
    }
}
