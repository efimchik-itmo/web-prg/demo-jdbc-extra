package com.epam.jdbc.extra.daorepo;

import com.epam.jdbc.extra.ConnectionSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoJdbcImpl implements UserDao {

    private final ConnectionSource connectionSource;

    public UserDaoJdbcImpl(final ConnectionSource connectionSource) {
        this.connectionSource = connectionSource;
    }

    @Override
    public void create(final User user) {
        try (final Connection connection = connectionSource.getConnection();
             final PreparedStatement ps = connection.prepareStatement(
                     "insert into USER VALUES ( ?, ?, ?)")
        ) {
            ps.setLong(1, user.getId());
            ps.setString(2, user.getUsername());
            ps.setString(3, user.getPassword());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public User read(final long id) {
        try (final Connection connection = connectionSource.getConnection();
             final PreparedStatement ps = connection.prepareStatement(
                     "select USERNAME, USERPASS from USER " +
                             "where USER_ID = ?")
        ) {
            ps.setLong(1, id);
            try (final ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    return new User(
                            id,
                            resultSet.getString(1),
                            resultSet.getString(2)
                    );
                }
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(final User user) {
        try (final Connection connection = connectionSource.getConnection();
             final PreparedStatement ps = connection.prepareStatement(
                     "update USER set USERNAME = ?, USERPASS = ? " +
                             "where USER_ID = ?")
        ) {
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ps.setLong(3, user.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(final long id) {
        try (final Connection connection = connectionSource.getConnection();
             final PreparedStatement ps = connection.prepareStatement(
                     "delete from USER where USER_ID = ?")
        ) {
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
