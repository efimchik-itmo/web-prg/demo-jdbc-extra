package com.epam.jdbc.extra.daorepo;

public interface SessionRepository {
    Session get(Long id);

    void add(Session session);

    void update(Session session);

    void remove(Session session);
}
