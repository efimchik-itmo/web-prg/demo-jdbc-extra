package com.epam.jdbc.extra.daorepo;

public interface UserDao {
    void create(User user);

    User read(long id);

    void update(User user);

    void delete(long id);
}
