package com.epam.jdbc.extra.stored;

public class IsAdminFunction {
    public static boolean isAdmin(String input) {
        return "admin".equalsIgnoreCase(input);
    }
}
