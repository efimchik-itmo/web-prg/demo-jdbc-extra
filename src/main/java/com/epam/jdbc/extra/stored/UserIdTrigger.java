package com.epam.jdbc.extra.stored;

import org.h2.tools.TriggerAdapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserIdTrigger extends TriggerAdapter {
    @Override
    public void fire(final Connection connection,
                     final ResultSet oldRow,
                     final ResultSet newRow) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement(
                "select NEXT VALUE FOR USER_ID_SEQ");
             ResultSet resultSet = ps.executeQuery()) {
            resultSet.next();
            final long id = resultSet.getLong(1);
            newRow.updateLong(1, id);
        }
    }
}
