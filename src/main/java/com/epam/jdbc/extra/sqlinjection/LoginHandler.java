package com.epam.jdbc.extra.sqlinjection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.OptionalLong;

public class LoginHandler {

    private final Connection conn;

    public LoginHandler(final Connection sqlConnection) {
        this.conn = sqlConnection;
    }

    public OptionalLong login(String username, String password) throws SQLException {
        try (PreparedStatement statement = conn.prepareStatement(
                "select USER_ID from USER " +
                        "where USERNAME = ? and USERPASS = ? ")) {

            statement.setString(1, username);
            statement.setString(2, password);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return OptionalLong.of(resultSet.getLong(1));
                }
            }
        }
        return OptionalLong.empty();
    }
}
